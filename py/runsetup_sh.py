import io
import os
import shutil
#import simfiles

alphaL = range(0,100)
alphaD = 0.3333333333
runname = "sep12"
user_email = "herman67@msu.edu"
restart = True

# should put in full path name if running sbatch
PP_DIR = "../"
SCRATCH_DIR = "/mnt/gs18/scratch/users/herman67/skynet_outputs/"
BUILD_DIR = PP_DIR + "build/"
OUT_DIR = SCRATCH_DIR

for a in alphaL:
    for it in range(5): 
        iteration = it
        print(iteration, a)
        path_temp_output = OUT_DIR + "outputs_100/run_"+runname+"_"+str(a)

        if not os.path.isdir(path_temp_output):
            os.makedirs(path_temp_output)
        path1 = PP_DIR + "/run_sh"
        if not os.path.isdir(path1):
            os.makedirs(path1)
        filename = "run"+str(a)+"_"+str(iteration)+".sh"
        fullpath = os.path.join(path1, filename)
        threads = "10"
        time = "3:30:00"
        if it == 0:
            threads = "1"
            time = "0:30:00"
        with io.FileIO(fullpath, "w") as file:
                file.write("#!/bin/bash -login\n")
                file.write("\n")
                file.write("### define resources needed:\n")
                file.write("### walltime - how long you expect the job to run\n")
                file.write("#SBATCH --time="+time+"\n")
                file.write("\n")
                file.write("### nodes:ppn - how many nodes & cores per node (ppn) that you require\n")
                file.write("#SBATCH --ntasks="+threads+" --constraint=\"lac\"\n")
                file.write("\n")
                file.write("### mem: amount of memory that the job will need\n")
                file.write("#SBATCH --mem-per-cpu=5G\n")
                file.write("#SBATCH --account=ptg\n")
                file.write("### you can give your job a name for easier identification\n")
                file.write("#SBATCH -J "+runname + "_" + str(a)+"_"+str(iteration)+"\n")
                file.write("\n")
                file.write("### error/output file specifications\n")
                file.write("#SBATCH -o "+path_temp_output+"/job_output"+str(iteration)+".txt\n")
                file.write("#SBATCH -e "+path_temp_output+"/job_output"+str(iteration)+".txt\n")
                file.write("### load necessary modules, e.g.\n")
                file.write("#SBATCH --mail-user="+user_email+"\n")
                file.write("#SBATCH --mail-type=FAIL\n")
                file.write("\n")
                file.write("### change to the working directory where your code is located\n")
                file.write("cd "+BUILD_DIR+"\n")
                file.write("### call your executable\n")
                array_i = "${SLURM_ARRAY_TASK_ID}" 
                file.write("srun -n "+threads+" fulltraj " + str(a) + " " + str(iteration) + "\n")
                file.write("cd "+PP_DIR+"py\n")
                if iteration != 0:
                    file.write("python3 cleanup_scratch.py "+str(a)+" "+str(a)+" "+str(iteration)+"\n")
                file.write("scontrol show job ${SLURM_JOB_ID}\n")
                if iteration > 0 and iteration < 4:
                    file.write("sbatch "+PP_DIR+"run_sh/run"+str(a)+"_"+str(iteration+1)+".sh\n")
                elif iteration==4 and a>29:
                    if a > 79:
                        file.write("sbatch "+PP_DIR+"run_sh/run"+str(a-80)+"_"+str(2)+".sh\n")
                    else:
                        file.write("sbatch "+PP_DIR+"run_sh/run"+str(a+20)+"_"+str(1)+".sh\n")
